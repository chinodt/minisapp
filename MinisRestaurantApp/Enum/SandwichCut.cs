﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinisRestaurantApp.Enum
{
    public enum SandwichCut
    {
        Square = 0,
        Round = 1,
        Triangle = 2
    }
}