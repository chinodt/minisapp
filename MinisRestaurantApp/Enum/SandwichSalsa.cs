﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinisRestaurantApp.Enum
{
    public enum SandwichSalsa
    {
        OnTop = 0,
        Inside = 1,
        NoSalsa = 2
    }
}