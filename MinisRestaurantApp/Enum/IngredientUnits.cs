﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinisRestaurantApp.Enum
{
    public enum IngredientUnits
    {
        Kilos = 0,
        Gramos = 1,
        Unidad = 2
    }
}