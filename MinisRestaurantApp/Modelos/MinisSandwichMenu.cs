//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MinisRestaurantApp.Modelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class MinisSandwichMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MinisSandwichMenu()
        {
            this.MinisOrders = new HashSet<MinisOrder>();
        }
    
        public int Id { get; set; }
        public int IdMinisSandwich { get; set; }
        public int IdMinisIngredients { get; set; }
        public bool IsVisible { get; set; }
    
        public virtual MinisIngredient MinisIngredient { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MinisOrder> MinisOrders { get; set; }
        public virtual MinisSandwich MinisSandwich { get; set; }
    }
}
