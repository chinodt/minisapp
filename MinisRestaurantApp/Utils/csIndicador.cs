﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace MinisRestaurantApp.Utils
{
    public class csIndicador
    {
        string apiUrl = "https://www.mindicador.cl/api/dolar";
        string jsonString = "{}";
        WebClient http = new WebClient();
        JavaScriptSerializer jss = new JavaScriptSerializer();

        public string getIndicator()
        {
            DateTime dateTime = DateTime.Now;

            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(-3);

            while (dateTime.DayOfWeek != DayOfWeek.Friday)
            {
                dateTime = dateTime.AddDays(1);
            }

            http.Headers.Add(HttpRequestHeader.Accept, "application/json");
            jsonString = http.DownloadString(apiUrl + "/" + dateTime.ToString("dd-MM-yyyy"));
            var indicatorsObject = jss.Deserialize<Dictionary<string, object>>(jsonString);

            Dictionary<string, Dictionary<string, string>> dailyIndicators = new Dictionary<string, Dictionary<string, string>>();

            int i = 0;
            foreach (var key in indicatorsObject.Keys.ToArray())
            {
                var item = indicatorsObject[key];

                if (item.GetType().FullName.Contains("System.Collections.Generic.Dictionary"))
                {
                    Dictionary<string, object> itemObject = (Dictionary<string, object>)item;
                    Dictionary<string, string> indicatorProp = new Dictionary<string, string>();

                    int j = 0;
                    foreach (var key2 in itemObject.Keys.ToArray())
                    {
                        indicatorProp.Add(key2, itemObject[key2].ToString());
                        j++;
                    }

                    dailyIndicators.Add(key, indicatorProp);
                }
                i++;
            }

            return (dailyIndicators["uf"]["valor"]);
        }
    }
}