﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinisRestaurantApp.Modelos;
using Newtonsoft.Json.Linq;
using MinisRestaurantApp.Enum;
using System.Data;
using System.Data.Entity;
using System.Web.Http.Cors;
using System.Dynamic;
namespace MinisRestaurantApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/MinisIngredients")]
    [EnableCors(origins: "http://localhost:57295", headers: "*", methods: "*")]
    public class MinisIngredientsController : ApiController
    {
        private MinisRestaurantAppEntities _db = new MinisRestaurantAppEntities();

        //get all ingredients
        [HttpGet]
        [AllowAnonymous]
        [Route("GetIngredients")]
        public IHttpActionResult GetIngredients()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var ingredients = _db.MinisIngredients.Where(s => s.Quantity > 0).ToList();
                string sUnits = "";
                var ingredientsObj = new List<dynamic>();
                foreach (var s in ingredients)
                {
                    switch ((IngredientUnits)s.Units)
                    {
                        case IngredientUnits.Gramos:
                            sUnits = "GRAMOS";
                            break;
                        case IngredientUnits.Kilos:
                            sUnits = "KILOS";
                            break;
                        case IngredientUnits.Unidad:
                            sUnits = "UNIDAD";
                            break;
                    }

                    dynamic ingredientObj = new ExpandoObject();
                    ingredientObj.ingredientId = s.Id;
                    ingredientObj.ingredientName = s.IngredientName;
                    ingredientObj.quantity = s.Quantity;
                    ingredientObj.units = sUnits;
                    ingredientsObj.Add(ingredientObj);
                }
                return Ok(ingredientsObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get ingredient by id
        [HttpGet]
        [AllowAnonymous]
        [Route("GetIngredientsById")]
        public IHttpActionResult GetIngredientsById(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                var ingredients = _db.MinisIngredients.Where(s => s.Id == id && s.Quantity > 0).FirstOrDefault();
                string sUnits = "";
                var ingredientsObj = new List<dynamic>();
                switch ((IngredientUnits)ingredients.Units)
                {
                    case IngredientUnits.Gramos:
                        sUnits = "GRAMOS";
                        break;
                    case IngredientUnits.Kilos:
                        sUnits = "KILOS";
                        break;
                    case IngredientUnits.Unidad:
                        sUnits = "UNIDAD";
                        break;
                }

                dynamic ingredientObj = new ExpandoObject();
                ingredientObj.ingredientId = ingredients.Id;
                ingredientObj.ingredientName = ingredients.IngredientName;
                ingredientObj.quantity = ingredients.Quantity;
                ingredientObj.units = sUnits;
                ingredientsObj.Add(ingredientsObj);
                return Ok(ingredientsObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //create ingredient
        [HttpPost]
        [AllowAnonymous]
        [Route("SaveIngredients")]
        public IHttpActionResult SaveIngredients(string ingredientName, int quantity, IngredientUnits unit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var ingredient = new MinisIngredient();
                ingredient.IngredientName = ingredientName;
                ingredient.Quantity = quantity;
                ingredient.Units = (int)unit;
                _db.Entry(ingredient).State = EntityState.Added;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Ingrediente Creado correctamente",
                        Nombre = ingredientName
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //update ingredient
        [HttpPut]
        [AllowAnonymous]
        [Route("UpdateIngredients")]
        public IHttpActionResult UpdateIngredients(int id, string ingredientName, int quantity, IngredientUnits unit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var ingredient = new MinisIngredient();
                ingredient.Id = id;
                ingredient.IngredientName = ingredientName;
                ingredient.Quantity = quantity;
                ingredient.Units = (int)unit;
                _db.Entry(ingredient).State = EntityState.Modified;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Ingrediente Modificado correctamente",
                        Nombre = ingredientName
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
