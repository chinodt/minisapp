﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinisRestaurantApp.Modelos;
using Newtonsoft.Json.Linq;
using MinisRestaurantApp.Enum;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Web.Http.Cors;
namespace MinisRestaurantApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/MinisSandwiches")]
    [EnableCors(origins: "http://localhost:57295", headers: "*", methods: "*")]
    public class MinisSandwichesController : ApiController
    {
        private MinisRestaurantAppEntities _db = new MinisRestaurantAppEntities();

        //get all sandwich
        [HttpGet]
        [AllowAnonymous]
        [Route("GetSandwiches")]
        public IHttpActionResult GetSandwiches()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwiches = _db.MinisSandwiches.Where(s => s.IsAvaiable == true).ToList();
                string sCut = "";
                string sSalsa = "";
                string sType = "";
                var sandwichesObj = new List<dynamic>();
                foreach (var s in sandwiches)
                {
                    switch ((SandwichCut) s.SandwichCut)
                    {
                        case SandwichCut.Round:
                            sCut = "ROUND";
                            break;
                        case SandwichCut.Square:
                            sCut = "SQUARE";
                            break;
                        case SandwichCut.Triangle:
                            sCut = "TRIANGLE";
                            break;
                    }

                    switch ((SandwichSalsa)s.SandwichSalsa)
                    {
                        case SandwichSalsa.Inside:
                            sSalsa = "INSIDE";
                            break;
                        case SandwichSalsa.NoSalsa:
                            sSalsa = "NO SALSA";
                            break;
                        case SandwichSalsa.OnTop:
                            sSalsa = "ON TOP";
                            break;
                    }

                    switch ((SandwichType)s.SandwichType)
                    {
                        case SandwichType.Dini:
                            sType = "DINI";
                            break;
                        case SandwichType.Mini:
                            sType = "MINI";
                            break;
                        case SandwichType.Pini:
                            sType = "PINI";
                            break;
                    }

                    dynamic sandwichObj = new ExpandoObject();
                    sandwichObj.sandwichId = s.Id;
                    sandwichObj.sandwichName = s.SandwichName;
                    sandwichObj.sandwichCut = sCut;
                    sandwichObj.sandwichSalsa = sSalsa;
                    sandwichObj.sandwichType = sType;
                    sandwichObj.sandwichCost = s.Cost;
                    sandwichObj.isCompress = s.IsCompress;
                    sandwichObj.isAvaiable = s.IsAvaiable;
                    sandwichesObj.Add(sandwichObj);
                }
                return Ok(sandwichesObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get sandwich by id
        [HttpGet]
        [AllowAnonymous]
        [Route("GetSandwichById")]
        public IHttpActionResult GetSandwichById(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwich = _db.MinisSandwiches.Where(s => s.Id==id && s.IsAvaiable == true).FirstOrDefault();

                var sandwichesObj = new List<dynamic>();
                dynamic sandwichObj = new ExpandoObject();
                sandwichObj.sandwichId = sandwich.Id;
                sandwichObj.sandwichName = sandwich.SandwichName;
                sandwichObj.sandwichCut = (SandwichCut)sandwich.SandwichCut;
                sandwichObj.sandwichSalsa = (SandwichSalsa)sandwich.SandwichSalsa;
                sandwichObj.sandwichType = (SandwichType)sandwich.SandwichType;
                sandwichObj.sandwichCost = sandwich.Cost;
                sandwichObj.isCompress = sandwich.IsCompress;
                sandwichObj.isAvaiable = sandwich.IsAvaiable;
                sandwichesObj.Add(sandwichObj);
                return Ok(sandwichesObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //create sandwich
        [HttpPost]
        [AllowAnonymous]
        [Route("SaveSandwich")]
        public IHttpActionResult SaveSandwich(string sandwichName, SandwichCut sandwichCut, SandwichSalsa sandwichSalsa,
            SandwichType sandwichType, decimal sandwichCost, bool isCompress, bool isAvaiable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwich = new MinisSandwich();
                sandwich.Cost = sandwichCost;
                sandwich.IsAvaiable = isAvaiable;
                sandwich.IsCompress = isCompress;
                sandwich.SandwichCut = (int)sandwichCut;
                sandwich.SandwichName = sandwichName;
                sandwich.SandwichSalsa = (int)sandwichSalsa;
                sandwich.SandwichType = (int)sandwichType;
                _db.Entry(sandwich).State = EntityState.Added;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Sandwich Creado correctamente",
                        Nombre = sandwichName
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //update sandwich
        [HttpPut]
        [AllowAnonymous]
        [Route("UpdateSandwich")]
        public IHttpActionResult UpdateSandwich(int sandwichId, string sandwichName, SandwichCut sandwichCut, SandwichSalsa sandwichSalsa,
            SandwichType sandwichType, decimal sandwichCost, bool isCompress, bool isAvaiable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwich = new MinisSandwich();
                sandwich.Id = sandwichId;
                sandwich.Cost = sandwichCost;
                sandwich.IsAvaiable = isAvaiable;
                sandwich.IsCompress = isCompress;
                sandwich.SandwichCut = (int)sandwichCut;
                sandwich.SandwichName = sandwichName;
                sandwich.SandwichSalsa = (int)sandwichSalsa;
                sandwich.SandwichType = (int)sandwichType;
                _db.Entry(sandwich).State = EntityState.Modified;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Sandwich Actualizado correctamente",
                        Nombre = sandwichName
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
