﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinisRestaurantApp.Modelos;
using Newtonsoft.Json.Linq;
using MinisRestaurantApp.Enum;
using System.Data;
using System.Data.Entity;

namespace MinisRestaurantApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/MinisOrders")]
    public class MinisOrdersController : ApiController
    {
        private MinisRestaurantAppEntities _db = new MinisRestaurantAppEntities();
        //get all orders
        [HttpGet]
        [AllowAnonymous]
        [Route("GetOrders")]
        public IHttpActionResult GetOrders()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var orders = _db.MinisOrders.OrderByDescending(x => x.OrderDate).ToList();

                return Ok(orders);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get orders by date
        [HttpGet]
        [AllowAnonymous]
        [Route("GetOrderByDate")]
        public IHttpActionResult GetOrderByDate(DateTime dateOrder)
        {
            var dateOrderSearch = new DateTime(dateOrder.Year, dateOrder.Month, dateOrder.Day);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var order = _db.MinisOrders.Where(s => s.OrderDate == dateOrderSearch).ToList();

                return Ok(order);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get orders by range of date 
        [HttpGet]
        [AllowAnonymous]
        [Route("GetOrdersByDates")]
        public IHttpActionResult GetOrdersByDates(DateTime dateOrderFrom, DateTime dateOrderTo)
        {
            var dateOrderSearchFrom = new DateTime(dateOrderFrom.Year, dateOrderFrom.Month, dateOrderFrom.Day);
            var dateOrderSearchTo = new DateTime(dateOrderTo.Year, dateOrderTo.Month, dateOrderTo.Day);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var order = _db.MinisOrders.Where(s => s.OrderDate >= dateOrderSearchFrom && s.OrderDate <= dateOrderSearchTo).ToList();

                return Ok(order);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get orders by date
        [HttpGet]
        [AllowAnonymous]
        [Route("GetOrderByDate")]
        public IHttpActionResult GetOrderById(int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var order = _db.MinisOrders.Where(s => s.Id == Id).FirstOrDefault();

                return Ok(order);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //create sandwich
        [HttpPost]
        [AllowAnonymous]
        [Route("SaveOrder")]
        public IHttpActionResult SaveOrder(int IdMinisSandwichMenu, int IdMinisBeverage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var order = new MinisOrder();
                order.IdMinisBeverage = IdMinisBeverage;
                order.IdMinisSandwichMenu = IdMinisSandwichMenu;
                order.OrderDate = DateTime.Now;
                _db.Entry(order).State = EntityState.Added;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Orden Creada correctamente",
                        Order = order.Id
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
