﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinisRestaurantApp.Modelos;
using Newtonsoft.Json.Linq;
using MinisRestaurantApp.Enum;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Web.Http.Cors;
using MinisRestaurantApp.Utils;
namespace MinisRestaurantApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/MinisSandwichMenu")]
    [EnableCors(origins: "http://localhost:57295", headers: "*", methods: "*")]
    public class MinisSandwichMenuController : ApiController
    {
        private MinisRestaurantAppEntities _db = new MinisRestaurantAppEntities();

        //get all orders
        [HttpGet]
        [AllowAnonymous]
        [Route("GetMinisSandwichMenu")]
        public IHttpActionResult GetMinisSandwichMenu()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var orders = _db.MinisSandwichMenus.Include(p => p.MinisIngredient).Include(o=> o.MinisSandwich).Where(x => x.IsVisible==true).ToList();
                string sCut = "";
                string sUnits = "";
                string sSalsa = "";
                string sType = "";
                var menuesObj = new List<dynamic>();
                foreach (var s in orders)
                {
                    switch ((SandwichCut)s.MinisSandwich.SandwichCut)
                    {
                        case SandwichCut.Round:
                            sCut = "ROUND";
                            break;
                        case SandwichCut.Square:
                            sCut = "SQUARE";
                            break;
                        case SandwichCut.Triangle:
                            sCut = "TRIANGLE";
                            break;
                    }

                    switch ((SandwichSalsa)s.MinisSandwich.SandwichSalsa)
                    {
                        case SandwichSalsa.Inside:
                            sSalsa = "INSIDE";
                            break;
                        case SandwichSalsa.NoSalsa:
                            sSalsa = "NO SALSA";
                            break;
                        case SandwichSalsa.OnTop:
                            sSalsa = "ON TOP";
                            break;
                    }

                    switch ((SandwichType)s.MinisSandwich.SandwichType)
                    {
                        case SandwichType.Dini:
                            sType = "DINI";
                            break;
                        case SandwichType.Mini:
                            sType = "MINI";
                            break;
                        case SandwichType.Pini:
                            sType = "PINI";
                            break;
                    }

                    switch ((IngredientUnits)s.MinisIngredient.Units)
                    {
                        case IngredientUnits.Gramos:
                            sUnits = "GRAMOS";
                            break;
                        case IngredientUnits.Kilos:
                            sUnits = "KILOS";
                            break;
                        case IngredientUnits.Unidad:
                            sUnits = "UNIDAD";
                            break;
                    }

                    dynamic menuObj = new ExpandoObject();
                    menuObj.Id = s.Id;
                    menuObj.sandwichName = s.MinisSandwich.SandwichName;
                    menuObj.sandwichCut = sCut;
                    menuObj.sandwichSalsa = sSalsa;
                    menuObj.sandwichType = sType;
                    menuObj.sandwichCost = s.MinisSandwich.Cost;
                    menuObj.isCompress = s.MinisSandwich.IsCompress;
                    menuObj.isAvaiable = s.MinisSandwich.IsAvaiable;
                    menuObj.ingredientName = s.MinisIngredient.IngredientName;
                    menuObj.quantity = s.MinisIngredient.Quantity;
                    menuObj.units = sUnits;
                    menuesObj.Add(menuObj);
                }
                return Ok(menuesObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get all orders
        [HttpGet]
        [AllowAnonymous]
        [Route("GetMinisSandwichMenuDollar")]
        public IHttpActionResult GetMinisSandwichMenuDollar()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var orders = _db.MinisSandwichMenus.Include(p => p.MinisIngredient).Include(o => o.MinisSandwich).Where(x => x.IsVisible == true).ToList();
                string sCut = "";
                string sUnits = "";
                string sSalsa = "";
                string sType = "";
                csIndicador csI = new csIndicador();
                var sVal = csI.getIndicator();
                double valDollar = double.Parse(csI.getIndicator());
                var menuesObj = new List<dynamic>();
                foreach (var s in orders)
                {
                    switch ((SandwichCut)s.MinisSandwich.SandwichCut)
                    {
                        case SandwichCut.Round:
                            sCut = "ROUND";
                            break;
                        case SandwichCut.Square:
                            sCut = "SQUARE";
                            break;
                        case SandwichCut.Triangle:
                            sCut = "TRIANGLE";
                            break;
                    }

                    switch ((SandwichSalsa)s.MinisSandwich.SandwichSalsa)
                    {
                        case SandwichSalsa.Inside:
                            sSalsa = "INSIDE";
                            break;
                        case SandwichSalsa.NoSalsa:
                            sSalsa = "NO SALSA";
                            break;
                        case SandwichSalsa.OnTop:
                            sSalsa = "ON TOP";
                            break;
                    }

                    switch ((SandwichType)s.MinisSandwich.SandwichType)
                    {
                        case SandwichType.Dini:
                            sType = "DINI";
                            break;
                        case SandwichType.Mini:
                            sType = "MINI";
                            break;
                        case SandwichType.Pini:
                            sType = "PINI";
                            break;
                    }

                    switch ((IngredientUnits)s.MinisIngredient.Units)
                    {
                        case IngredientUnits.Gramos:
                            sUnits = "GRAMOS";
                            break;
                        case IngredientUnits.Kilos:
                            sUnits = "KILOS";
                            break;
                        case IngredientUnits.Unidad:
                            sUnits = "UNIDAD";
                            break;
                    }

                    dynamic menuObj = new ExpandoObject();
                    menuObj.Id = s.Id;
                    menuObj.sandwichName = s.MinisSandwich.SandwichName;
                    menuObj.sandwichCut = sCut;
                    menuObj.sandwichSalsa = sSalsa;
                    menuObj.sandwichType = sType;
                    menuObj.sandwichCost = s.MinisSandwich.Cost;
                    menuObj.sandwichDollarCost = s.MinisSandwich.Cost;
                    menuObj.isCompress = s.MinisSandwich.IsCompress;
                    menuObj.isAvaiable = s.MinisSandwich.IsAvaiable;
                    menuObj.ingredientName = s.MinisIngredient.IngredientName;
                    menuObj.quantity = s.MinisIngredient.Quantity;
                    menuObj.units = sUnits;
                    menuesObj.Add(menuObj);
                }
                return Ok(menuesObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //create sandwich
        [HttpPost]
        [AllowAnonymous]
        [Route("SaveMinisSandwichMenu")]
        public IHttpActionResult SaveMinisSandwichMenu(int idMinisSandwich, int idMinisIngredients)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var minisSandwichMenu = new MinisSandwichMenu();
                minisSandwichMenu.IdMinisSandwich = idMinisSandwich;
                minisSandwichMenu.IdMinisIngredients = idMinisIngredients;
                minisSandwichMenu.IsVisible = true;
                _db.Entry(minisSandwichMenu).State = EntityState.Added;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Registro Creado correctamente",
                        Id = minisSandwichMenu.Id
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //update sandwich
        [HttpPut]
        [AllowAnonymous]
        [Route("RemoveMinisSandwichMenu")]
        public IHttpActionResult RemoveMinisSandwichMenu(int id, int idMinisSandwich, int idMinisIngredients)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var minisSandwichMenu = new MinisSandwichMenu();
                minisSandwichMenu.Id = id;
                minisSandwichMenu.IdMinisSandwich = idMinisSandwich;
                minisSandwichMenu.IdMinisIngredients = idMinisIngredients;
                minisSandwichMenu.IsVisible = false;
                _db.Entry(minisSandwichMenu).State = EntityState.Modified;
                _db.SaveChanges();

                return Ok(
                    new
                    {
                        Mensaje = "Registro Deshabilitado correctamente",
                        Id = minisSandwichMenu.Id
                    }
                );
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get all ingredients
        [HttpGet]
        [AllowAnonymous]
        [Route("GetIngredients")]
        public IHttpActionResult GetIngredients()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var ingredients = _db.MinisIngredients.Where(s => s.Quantity > 0).ToList();
                var ingredientsObj = new List<dynamic>();
                foreach (var s in ingredients)
                {
                    dynamic ingredientObj = new ExpandoObject();
                    ingredientObj.ingredientId = s.Id;
                    ingredientObj.ingredientName = s.IngredientName;
                    ingredientsObj.Add(ingredientObj);
                }
                return Ok(ingredientsObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get all sandwich
        [HttpGet]
        [AllowAnonymous]
        [Route("GetSandwiches")]
        public IHttpActionResult GetSandwiches()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwiches = _db.MinisSandwiches.Where(s => s.IsAvaiable == true).ToList();
                string sCut = "";
                string sSalsa = "";
                string sType = "";
                var sandwichesObj = new List<dynamic>();
                foreach (var s in sandwiches)
                {
                    switch ((SandwichCut)s.SandwichCut)
                    {
                        case SandwichCut.Round:
                            sCut = "ROUND";
                            break;
                        case SandwichCut.Square:
                            sCut = "SQUARE";
                            break;
                        case SandwichCut.Triangle:
                            sCut = "TRIANGLE";
                            break;
                    }

                    switch ((SandwichSalsa)s.SandwichSalsa)
                    {
                        case SandwichSalsa.Inside:
                            sSalsa = "INSIDE";
                            break;
                        case SandwichSalsa.NoSalsa:
                            sSalsa = "NO SALSA";
                            break;
                        case SandwichSalsa.OnTop:
                            sSalsa = "ON TOP";
                            break;
                    }

                    switch ((SandwichType)s.SandwichType)
                    {
                        case SandwichType.Dini:
                            sType = "DINI";
                            break;
                        case SandwichType.Mini:
                            sType = "MINI";
                            break;
                        case SandwichType.Pini:
                            sType = "PINI";
                            break;
                    }

                    dynamic sandwichObj = new ExpandoObject();
                    sandwichObj.sandwichId = s.Id;
                    sandwichObj.sandwichName = s.SandwichName + ", Cut: " + sCut + ", Type: " + sType + ", Salsa: " + sSalsa;
                    sandwichesObj.Add(sandwichObj);
                }
                return Ok(sandwichesObj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
