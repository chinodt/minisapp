﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinisRestaurantApp.Modelos;
using Newtonsoft.Json.Linq;
using MinisRestaurantApp.Enum;
using System.Data;
using System.Data.Entity;

namespace MinisRestaurantApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/MinisBeverage")]
    public class MinisBeverageController : ApiController
    {
        private MinisRestaurantAppEntities _db = new MinisRestaurantAppEntities();
        //get all beverages
        [HttpGet]
        [AllowAnonymous]
        [Route("GetBeverages")]
        public IHttpActionResult GetBeverages()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwiches = _db.MinisBeverages .Where(s => s.IsAvaiable==true).ToList();

                return Ok(sandwiches);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        //get all beverages
        [HttpGet]
        [AllowAnonymous]
        [Route("GetBeverageById")]
        public IHttpActionResult GetBeverageById(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sandwiches = _db.MinisBeverages.Where(s => s.Id == id && s.IsAvaiable == true).FirstOrDefault();

                return Ok(sandwiches);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
