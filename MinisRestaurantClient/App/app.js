﻿'use strict';
// Declares how the application should be bootstrapped. See: http://docs.angularjs.org/guide/module
angular.module('app',
    [
        // Dependency Inject Goes inside this array
        'ui.router',  // we inject/load ui-router for routing
        'app.controllers', // we inject/load the controllers
    ]
)
    .config(['$stateProvider',
        function ($stateProvider) {
            // UI States, URL Routing & Mapping. For more info see: https://github.com/angular-ui/ui-router

            // our routers, self explanatory
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: './Views/Home.html'
                })
                .state('Sandwich', {
                    url: '/Sandwich',
                    templateUrl: './Views/Admin/Sandwiches.html',
                    controller: 'minisSandwichesController'
                })
                .state('Ingredients', {
                    url: '/Ingredients',
                    templateUrl: './Views/Admin/Ingredients.html',
                    controller: 'minisIngredientsController'
                })
                .state('SandwichMenu', {
                    url: '/SandwichMenu',
                    templateUrl: './Views/Admin/Menu.html',
                    controller: 'minisSandwichMenuController'
                })
                .state('ClientMenu', {
                    url: '/ClientMenu',
                    templateUrl: './Views/Client/Menu.html',
                    controller: 'minisSandwichMenuController'
                })
        }]

        );
