﻿angular.module('app.controllers', [])
.controller('minisSandwichesController', [
        '$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {

            //definicion de variables.
            $scope.loading = true;
            $scope.addMode = false;
            $scope.messageOk = "";
            $scope.messageError = "";
            $scope.allReports = [];
            $scope.itemsRpt = [];
            //definicion de metodos
            $scope.buscaSandwiches = function () {
                $scope.allObjects = [];
                $scope.getSandwiches().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            $scope.guardaSandwich = function (sandwich) {
                $scope.filteredObjects = [];
                $scope.allObjects = [];
                $scope.saveSandwich(sandwich).then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                        alert("Added Successfully!!");
                        $scope.addMode = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }
                );
            }

            $scope.getSandwiches = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwiches/GetSandwiches',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.getSandwichById = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwiches/GetSandwichById',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        id: id
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //by pressing toggleEdit button ng-click in html, this method will be hit
            $scope.toggleEdit = function () {
                this.sandwich.editMode = !this.sandwich.editMode;
            };

            //by pressing toggleAdd button ng-click in html, this method will be hit
            $scope.toggleAdd = function () {
                $scope.addMode = !$scope.addMode;
            };

            $scope.saveSandwich = function (sandwich) {
                return $http({
                    method: 'POST',
                    url: 'http://localhost:49256/api/MinisSandwiches/SaveSandwich',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        sandwichName: sandwich.sandwichName,
                        sandwichCut: sandwich.sandwichCut,
                        sandwichSalsa: sandwich.sandwichSalsa,
                        sandwichType: sandwich.sandwichType,
                        sandwichCost: sandwich.sandwichCost,
                        isCompress: sandwich.isCompress,
                        isAvaiable: sandwich.isAvaiable
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.updateSandwich = function (sandwich) {
                return $http({
                    method: 'PUT',
                    url: 'http://localhost:49256/api/MinisSandwiches/UpdateSandwich',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        sandwichId: sandwich.sandwichId,
                        sandwichName: sandwich.sandwichName,
                        sandwichCut: sandwich.sandwichCut,
                        sandwichSalsa: sandwich.sandwichSalsa,
                        sandwichType: sandwich.sandwichType,
                        sandwichCost: sandwich.sandwichCost,
                        isCompress: sandwich.isCompress,
                        isAvaiable: sandwich.isAvaiable
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //metodo Inicio.
            $scope.init = function () {
                $scope.buscaSandwiches();
            };
            $scope.init();
        }
]);