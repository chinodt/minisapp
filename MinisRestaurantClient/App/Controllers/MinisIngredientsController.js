﻿angular.module('app.controllers')
.controller('minisIngredientsController', [
        '$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {

            //definicion de variables.
            $scope.loading = true;
            $scope.addMode = false;
            $scope.messageOk = "";
            $scope.messageError = "";
            $scope.allReports = [];
            $scope.itemsRpt = [];
            //definicion de metodos
            $scope.buscaIngredients = function () {
                $scope.allObjects = [];
                $scope.getIngredients().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            $scope.guardaIngredient = function (ingredient) {
                $scope.filteredObjects = [];
                $scope.allObjects = [];
                $scope.saveIngredient(ingredient).then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                        alert("Added Successfully!!");
                        $scope.addMode = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }
                );
            }

            $scope.getIngredients = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisIngredients/GetIngredients',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.getSandwichById = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisIngredients/GetIngredientsById',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        id: id
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //by pressing toggleEdit button ng-click in html, this method will be hit
            $scope.toggleEdit = function () {
                this.sandwich.editMode = !this.sandwich.editMode;
            };

            //by pressing toggleAdd button ng-click in html, this method will be hit
            $scope.toggleAdd = function () {
                $scope.addMode = !$scope.addMode;
            };

            $scope.saveIngredient = function (ingredient) {
                return $http({
                    method: 'POST',
                    url: 'http://localhost:49256/api/MinisIngredients/SaveIngredients',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        ingredientName: ingredient.ingredientName,
                        quantity: ingredient.quantity,
                        unit: ingredient.unit
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.updateIngredient = function (ingredient) {
                return $http({
                    method: 'PUT',
                    url: 'http://localhost:49256/api/MinisIngredients/UpdateIngredients',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        id: ingredient.id,
                        ingredientName: ingredient.ingredientName,
                        quantity: ingredient.quantity,
                        unit: ingredient.unit
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //metodo Inicio.
            $scope.init = function () {
                $scope.buscaIngredients();
            };
            $scope.init();
        }
]);