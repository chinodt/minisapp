﻿angular.module('app.controllers')
.controller('minisSandwichMenuController', [
        '$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {

            //definicion de variables.
            $scope.loading = true;
            $scope.addMode = false;
            $scope.messageOk = "";
            $scope.messageError = "";
            $scope.allReports = [];
            $scope.itemsRpt = [];
            //definicion de metodos
            $scope.buscaSandwichMenu = function () {
                $scope.allObjects = [];
                $scope.getSandwichMenu().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            $scope.buscaSandwichMenuDollar = function () {
                $scope.allObjectsDollar = [];
                $scope.getSandwichMenuDollar().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjectsDollar.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            //definicion de metodos
            $scope.buscaSandwich = function () {
                $scope.allObjects = [];
                $scope.getSandwiches().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.Sandwich.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            //definicion de metodos
            $scope.buscaIngredient = function () {
                $scope.allObjects = [];
                $scope.getIngredients().then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.Ingredient.push(response.data[i]);
                        }
                        $scope.loading = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }

                );
            }

            $scope.guardaSandwichMenu = function (sandwichMenu) {
                $scope.filteredObjects = [];
                $scope.allObjects = [];
                $scope.saveSandwichMenu(sandwichMenu).then(
                    function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.allObjects.push(response.data[i]);
                        }
                        $scope.loading = false;
                        alert("Added Successfully!!");
                        $scope.addMode = false;
                    },
                    function (error) {
                        $scope.error = "An Error has occured!";
                        $scope.loading = false;
                    }
                );
            }

            $scope.getSandwichMenu = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwichMenu/GetMinisSandwichMenu',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.getSandwichMenuDollar = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwichMenu/GetMinisSandwichMenuDollar',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.getIngredients = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwichMenu/GetIngredients',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.getSandwiches = function () {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:49256/api/MinisSandwichMenu/GetSandwiches',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //by pressing toggleEdit button ng-click in html, this method will be hit
            $scope.toggleEdit = function () {
                this.sandwich.editMode = !this.sandwich.editMode;
            };

            //by pressing toggleAdd button ng-click in html, this method will be hit
            $scope.toggleAdd = function () {
                $scope.addMode = !$scope.addMode;
            };

            $scope.saveSandwichMenu = function (sandwichMenu) {
                return $http({
                    method: 'POST',
                    url: 'http://localhost:49256/api/MinisSandwichMenu/SaveMinisSandwichMenu',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        idMinisSandwich: sandwichMenu.idMinisSandwich,
                        idMinisIngredients: sandwichMenu.idMinisIngredients
                    }
                }).then(function (response) {
                    return response;
                });
            }

            $scope.updateIngredient = function (ingredient) {
                return $http({
                    method: 'PUT',
                    url: 'http://localhost:49256/api/MinisIngredients/UpdateIngredients',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        id: ingredient.id,
                        ingredientName: ingredient.ingredientName,
                        quantity: ingredient.quantity,
                        unit: ingredient.unit
                    }
                }).then(function (response) {
                    return response;
                });
            }

            //metodo Inicio.
            $scope.init = function () {
                $scope.buscaSandwichMenu();
                $scope.buscaSandwichMenuDollar();
                $scope.buscaSandwich();
                $scope.buscaIngredient();
            };
            $scope.init();
        }
]);